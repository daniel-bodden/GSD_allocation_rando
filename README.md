This project provides the files and code for the simulations of the paper "Standard Group Sequential Designs and Randomization with respect to Rare Disease Clinical Trials"
by Daniel Bodden and Ralf-Dieter Hilgers.

The procedure to calculate the type I error for different group sequential designs and randomization procedures which can be found in GSD-allocation.R, is currently being implemented in
the [randomizeR](https://cran.r-project.org/web/packages/randomizeR/index.html) package. In the main.R file of this project the code to create the boxplots
and tables for the paper can be found. The GSD-allocation.R files provides the iteration over all the randomization procedures and the group sequential designs of Pocock and O'Brien & Fleming, respectively, to calculate the T1E for every combination.
